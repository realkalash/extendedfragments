package com.example.fragments.utils;

import com.example.fragments.models.DrinksModel;
import com.example.fragments.models.RequestModel;

import java.util.ArrayList;
import java.util.List;

public class Convertor {
    public static List<DrinksModel> convertRequest(RequestModel requestModels) {

        List<DrinksModel> drinksModels = new ArrayList<>();
        List<RequestModel.Drink> drinks = requestModels.getDrinks();

        for (int i = 0; i < drinks.size(); i++) {
            drinksModels.add(new DrinksModel(
                    drinks.get(i).getIdDrink(),
                    drinks.get(i).getStrDrink(),
                    drinks.get(i).getStrDrinkThumb()
            ));
        }
        return drinksModels;
    }
}