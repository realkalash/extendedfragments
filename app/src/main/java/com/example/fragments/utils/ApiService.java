package com.example.fragments.utils;

import com.example.fragments.models.RequestModel;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://www.thecocktaildb.com/api/json/v1/1/";
    private static PrivateApi privateApi;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }


    public static Observable<RequestModel> getListOfDrinks(String textA) {
        return privateApi.getAllDrinks(textA);
    }

    public static Observable<RequestModel> getDrink(String idDrink) {
        return privateApi.getDrink(idDrink);
    }


    public interface PrivateApi {
        @GET("filter.php")
        Observable<RequestModel> getAllDrinks(@Query("a") String textA);

        @GET("lookup.php")
        Observable<RequestModel> getDrink(@Query("i") String idDrink);
    }
}
