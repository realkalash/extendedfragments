package com.example.fragments;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.fragments.controllers.FragmentDrinkInfo;
import com.example.fragments.controllers.FragmentListDrinks;
import com.example.fragments.interfaces.Coordinator;
import com.example.fragments.models.DrinksModel;
import com.example.fragments.models.RequestModel;

public class MainActivity extends AppCompatActivity implements Coordinator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState == null) {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                initPortrait();
            } else {
                initLandscape();
            }
        }

    }

    private void initLandscape() {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, FragmentListDrinks.newInstance())
                .commit();


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rootRight, new FragmentDrinkInfo())
                .commit();
    }

    private void initPortrait() {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new FragmentListDrinks())
                .commit();
    }



    @Override
    public void showNextFragment(DrinksModel drink) {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.root, FragmentDrinkInfo.newInstance(drink))
                    .commit();
        }else {

            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.rootRight, FragmentDrinkInfo.newInstance(drink))
                    .commit();
        }
    }
}

