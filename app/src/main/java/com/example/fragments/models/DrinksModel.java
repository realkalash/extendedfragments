package com.example.fragments.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DrinksModel implements Parcelable {
    @PrimaryKey
    String id;

    String nameDrink;
    String thumbnail;

    public DrinksModel(String id,String nameDrink, String thumbnail) {
        this.id = id;
        this.nameDrink = nameDrink;
        this.thumbnail = thumbnail;
    }


    protected DrinksModel(Parcel in) {
        id = in.readString();
        nameDrink = in.readString();
        thumbnail = in.readString();
    }

    public static final Creator<DrinksModel> CREATOR = new Creator<DrinksModel>() {
        @Override
        public DrinksModel createFromParcel(Parcel in) {
            return new DrinksModel(in);
        }

        @Override
        public DrinksModel[] newArray(int size) {
            return new DrinksModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getNameDrink() {
        return nameDrink;
    }

    public void setNameDrink(String nameDrink) {
        this.nameDrink = nameDrink;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(nameDrink);
        dest.writeString(thumbnail);
    }
}
