package com.example.fragments.controllers;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fragments.R;
import com.example.fragments.adapters.AdapterListDrinks;
import com.example.fragments.interfaces.Coordinator;
import com.example.fragments.models.DrinksModel;
import com.example.fragments.models.RequestModel;
import com.example.fragments.utils.ApiService;
import com.example.fragments.utils.Convertor;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class FragmentListDrinks extends Fragment implements Coordinator , AdapterListDrinks.OnDrinkClickListener {

    @BindView(R.id.recyclerListDrinks)
    RecyclerView recyclerView;

    private Coordinator coordinator;

    List<DrinksModel> drinks;


    public static FragmentListDrinks newInstance() {
        Bundle args = new Bundle();
        FragmentListDrinks fragment = new FragmentListDrinks();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        coordinator = (Coordinator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_drinks, container, false);
        ButterKnife.bind(this, view);

        Disposable disposable = ApiService.getListOfDrinks("Alcoholic")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<RequestModel>() {
                    @Override
                    public void accept(RequestModel requestModel) throws Exception {
                        drinks = Convertor.convertRequest(requestModel);

                        AdapterListDrinks adapterListDrinks = new AdapterListDrinks(drinks, getContext(),FragmentListDrinks.this);

                        recyclerView.setAdapter(adapterListDrinks);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                });

        return view;
    }


    @Override
    public void showNextFragment(DrinksModel drink) {
        coordinator.showNextFragment(drink);
    }

    @Override
    public void onItemClick(DrinksModel drinksModel) {
        showNextFragment(drinksModel);
    }
}
