package com.example.fragments.controllers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.fragments.R;
import com.example.fragments.models.DrinksModel;
import com.example.fragments.models.RequestModel;
import com.example.fragments.utils.ApiService;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import jp.co.cyberagent.android.gpuimage.GPUImageView;


public class FragmentDrinkInfo extends Fragment{

    @BindView(R.id.fdi_nameDrink)
    TextView nameDrink;
    @BindView(R.id.fdi_image)
    ImageView imageView;
    @BindView(R.id.fdi_descr)
    TextView descriptionDrink;

    DrinksModel drinksModel;

    public static FragmentDrinkInfo newInstance(DrinksModel drinksModel) {
        Bundle args = new Bundle();
        FragmentDrinkInfo fragment = new FragmentDrinkInfo();

        args.putParcelable("key", drinksModel); /* Запихиваем ОБЬЕКТ в бандл. Для этого Обьекту нужно имплементировать парсейбл */

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        if (arg != null)
        drinksModel = (DrinksModel) arg.getParcelable("key");
        else drinksModel = new DrinksModel("","", "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drink_info, container, false);
        ButterKnife.bind(this,view);

        ApiService.getDrink(drinksModel.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<RequestModel>() {
                    @Override
                    public void accept(RequestModel requestModel) throws Exception {
                        nameDrink.setText(requestModel.getDrinks().get(0).getStrDrink());
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(requestModel.getDrinks().get(0).getStrCategory());
                        String s = stringBuilder.toString();
                        descriptionDrink.setText(s);
                    }
                });

        Glide.with(getContext()).load(drinksModel.getThumbnail()).error(R.drawable.loading).placeholder(R.drawable.loading).into(imageView);

        return view;
    }

}
