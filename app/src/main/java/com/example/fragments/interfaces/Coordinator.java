package com.example.fragments.interfaces;

import com.example.fragments.models.DrinksModel;

public interface Coordinator {
    void showNextFragment(DrinksModel drink);
}
