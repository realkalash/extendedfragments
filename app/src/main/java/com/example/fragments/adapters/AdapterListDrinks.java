package com.example.fragments.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fragments.R;
import com.example.fragments.models.DrinksModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListDrinks extends RecyclerView.Adapter<AdapterListDrinks.ViewHolder> {

    List<DrinksModel> drinks;
    Context context;
    OnDrinkClickListener listener;

    public AdapterListDrinks(List<DrinksModel> drinks, Context context, OnDrinkClickListener listener) {
        this.drinks = drinks;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return drinks.size();
    }

    public interface OnDrinkClickListener {
        void onItemClick(DrinksModel drinksModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_nameDrink)
        TextView nameDrink;
        @BindView(R.id.item_imageDrink)
        ImageView imageDrink;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final int position) {
            nameDrink.setText(drinks.get(position).getNameDrink());

            Glide.with(context)
                    .load(drinks.get(position).getThumbnail())
                    .error(new ColorDrawable(Color.RED))
                    .placeholder(R.drawable.loading)
                    .into(imageDrink);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(drinks.get(position));
                }
            });
        }
    }
}
